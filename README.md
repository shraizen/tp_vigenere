## Projet IUT Vigenere Ciphering

First you need to install node dependencies :
    -npm install

Then you can run the script you want.
To run the main script :
    -npm run main

To run the testing script :
    -npm run test

Results are written in the console.

## Concerning the main script :
If you want to change the ciphered string and the keyword, do it directly in index.js
by replacing str and keyword values

const Vigenere = require('./vigenere.js'); 

test('Doit chiffrer un string', () => {

    expect( Vigenere.cipherText("ABCDEFGHIJKLMNOPQRSTUVWXYZ","RANDOMKEY") ).toEqual("RBPGSRQLGAKYPBAZUPJTHYKJID");
    expect( Vigenere.cipherText("MACHINCHOSE","RANDOMKEY") ).toEqual("DAPKWZMLMJE");
});

test('Doit chiffrer un string contenant des minuscules et majuscules', () => {

    expect( Vigenere.cipherText("AbCdEfGhIjKlMnOpQrStUvWxYz", "RANDOMKEY") ).toEqual("RHPMSXQRGGKEPHAFUVJZHEKPIJ");
});

test('Doit déchiffrer un string', () => {

    expect( Vigenere.originalText("RBPGSRQLGAKYPBAZUPJTHYKJID", Vigenere.generateKey("ABCDEFGHIJKLMNOPQRSTUVWXYZ","RANDOMKEY")) ).toEqual("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    expect( Vigenere.originalText("DAPKWZMLMJE", Vigenere.generateKey("MACHINCHOSE","RANDOMKEY")) ).toEqual("MACHINCHOSE");

});


test('Doit déchiffrer un string chiffré', () => {

    const originalString = "Ceci est un test";
    const cipheringKey = "machin";
    const cipheredString = Vigenere.cipherText(originalString, cipheringKey);
    expect( Vigenere.originalText(cipheredString, cipheringKey) ).toEqual(originalString);
});

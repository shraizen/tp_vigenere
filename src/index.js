const Vigenere = require('./vigenere.js')
const open = require('open')

let str = "UNECHAINEDECARACTERES";
let keyword = "RANDOMKEY";

let key = Vigenere.generateKey(str, keyword);
let cipher_text = Vigenere.cipherText(str, key);
    
    
console.log("Ciphertext : "+ cipher_text );
console.log("Original/Decrypted Text : "+ Vigenere.originalText(cipher_text, key));


open("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
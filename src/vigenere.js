// Fonction genere une clef jusqu'a ce qu'elle soit de la bonne taille
function generateKey(str,key)
{
	
	key=key.split("");
	if(str.length == key.length)
		return key.join("");
	else
	{
		let temp=key.length;
		for (let i = 0;i<(str.length-temp) ; i++)
		{
			
			key.push(key[i % ((key).length)])
		}
	}
	return key.join("");
}

//Cette fonction retourne le texte chiffré grace à la clef
function cipherText(str,key)
{
	let cipher_text="";

	for (let i = 0; i < str.length; i++)
	{
		// converting in range 0-25
		let x = (str[i].charCodeAt(0) + key[i].charCodeAt(0)) %26;

		// convert into alphabets(ASCII)
		x += 'A'.charCodeAt(0);

		cipher_text+=String.fromCharCode(x);
	}
	return cipher_text;
}

//Cette fonction déchiffre le texte chiffré et retourne le texte original
function originalText(cipher_text,key)
{
	let orig_text="";

	for (let i = 0 ; i < cipher_text.length ; i++)
	{
		// converting in range 0-25
		let x = (cipher_text[i].charCodeAt(0) -
					key[i].charCodeAt(0) + 26) %26;

		// convert into alphabets(ASCII)
		x += 'A'.charCodeAt(0);
		orig_text+=String.fromCharCode(x);
	}
	return orig_text;
}

//Cette fonction transforme toutes les lettres minuscules en majuscules
function LowerToUpper(s)
{
	let str =(s).split("");
	for(let i = 0; i < s.length; i++)
	{
		if(s[i] == s[i].toLowerCase())
		{
			str[i] = s[i].toUpperCase();
		}
	}
	s = str.toString();
	return s;
}
module.exports = { generateKey, cipherText, originalText, LowerToUpper};